import {delay, exitHandler, generateRandomHash, loadDummyData, prepareRocksdbQuery} from './utils';
import {writeFile, readFile} from 'fs/promises';
import {performance} from 'perf_hooks';
import {hrtime} from 'node:process';

const osu = require('node-os-utils');
const RocksDB = require('rocksdb');
const filesize = require('filesize');
const shell = require('shelljs');

const db = new RocksDB('./db');

const dbOptions = {
  createIfMissing: true,
  maxOpenFiles: 50000,
  writeBufferSize: 1024 * 1024 * 1024,
  maxFileSize: 1024 * 1024 * 1024,
};

const NS_PER_SEC = 1e9;
const t0 = Date.now();
let txc = 0;
let SHOULD_RUN_LOOP = true;
let timeTakenToInsertAllRecord = 0;
db.open(dbOptions, async (err: unknown) => {
  if (err) console.log('Err', err);

  console.log('Database Opened...');
  console.log(dbOptions);
  console.log('"timeTakenToInsertAllRecords","txc","tps","tInsert"');
  let data = await loadDummyData('./big_sample.json');
  data = JSON.stringify(data);
  const bulkSize = 1;

  // eslint-disable-next-line no-constant-condition
  while (SHOULD_RUN_LOOP) {
    // this is required to be generated at each iteration, unique id generation inside it.
    // if this line is move outside of loop, id are generated once
    // the higher the value the more it effect the accuracy of the test
    const query = prepareRocksdbQuery(bulkSize, data);

    const p0 = performance.now();
    db.batch(query, async (err: unknown) => {
      if (err) {
        throw Error('Insert Error');
      }
      txc += bulkSize;

      //high precision insert performance timer in nanosecond
      // const pdiff = hrtime(p0); // returns [x, y] -> x is second, and y is nanosecond
      const pfinal = performance.now() - p0;
      timeTakenToInsertAllRecord += pfinal;
      // reference: https://nodejs.org/api/process.html#process_process_hrtime_time

      //every interval of tx record data and pipe it to stdout
      if (txc % (bulkSize * 3000) === 0) {
        // accurate tps calculation
        const timeTakenToInsertAllRecordSEC = timeTakenToInsertAllRecord / 1000;
        const tps = (txc / timeTakenToInsertAllRecordSEC).toFixed(1);
        console.log(`"${Math.round(timeTakenToInsertAllRecordSEC)}s","${txc}","${tps}","${pfinal.toFixed(2)}ms"`);
      }
    });
    await delay(5);
  }
});

exitHandler(async () => {
  SHOULD_RUN_LOOP = false;

  const timelapsed = (Date.now() - t0) / 1000;
  const mem = filesize(process.memoryUsage().rss, {round: 0});
  const cpu = await osu.cpu.usage();
  const size = shell.exec('du -sh ./db', {silent: true}).stdout.replace('./db', '\r').trim();
  console.log('Final hardware profiles..');
  console.log(`timelapsed:${timelapsed}s, mem:${mem}, cpu:${cpu}%, dbsize:${size}`);

  console.log('Shutting down database...');
  db.close((err: unknown) => {
    // This happens second
    if (err) {
      throw Error('Database shutdown failure');
    }
    console.log('Database shutdown...');
  });
  // eslint-disable-next-line no-process-exit
  process.exit();
});
