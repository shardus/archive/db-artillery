import {promises as fs} from 'fs';
const RocksDB = require('rocksdb');
const vorpal = require('vorpal')();
const db = new RocksDB('./db');
const utils = require('./utils.ts');
const util = require('util');

const dbOptions = {
  createIfMissing: false,
  maxOpenFiles: 50000,
  writeBufferSize: 1024 * 1024 * 1024,
  maxFileSize: 1024 * 1024 * 1024,
};

// db.open(dbOptions, async err => {
//   if (err) throw err;
//   for await (const [key, value] of db.iterator()) {
//     const processed_value = JSON.parse(value.toString()) ? JSON.parse(value.toString()) : value.toString();
//     // console.log(util.inspect(processed_value, {showHidden: false, depth: null, colors: true}));
//     console.log(key.toString());
//     // console.log(processed_value)
//   }
// });

vorpal
  .command(
    'populate <size>',
    'This command create a sample database with key-value pair using the big_sample.json.<size> is how many items you want insert'
  )
  .action((args: any, callback: any) => {
    const options = {
      // eslint-disable-next-line node/no-unsupported-features/es-syntax
      ...dbOptions,
      errorIfExists: true,
    };
    options.createIfMissing = true;
    vorpal.log(options);
    db.open(options, async (err: any) => {
      if (err) {
        vorpal.log('[x] cannot populate db because directory already exist');
        db.close(() => {});
        callback();
      }
      if (!err) {
        let data = await utils.loadDummyData('./big_sample.json');
        data = JSON.stringify(data);
        for (let i = 0; i < args.size; i++) {
          db.put(utils.generateRandomHash(), data, (err: any) => {
            if (err) {
              throw Error(err);
            }
          });
        }
        db.close(() => {});
        vorpal.log('[+] Db population successful');
      }
    });
    callback();
  });

vorpal.command('keys', 'print all the keys inside the db').action((args: any, callback: Function) => {
  db.open(dbOptions, async (err: any) => {
    if (err) {
      vorpal.log(`[x] ${err}`);
      db.close(() => {});
      callback();
    }
    if (!err) {
      for await (const [key, value] of db.iterator()) {
        vorpal.log(key.toString());
      }
    }
    db.close(() => {});
  });
  callback();
});

vorpal
  .command(
    'dump <key> <path>',
    'dump the value of the key to a file, Example: dump f098bdb6c12766a000c7b6c5bed7293a4854d6d62ab615cd79401d610fc76fff tmp.json'
  )
  .action((args: any, callback: Function) => {
    db.open(dbOptions, async (err: any) => {
      if (err) {
        db.close(() => {});
        throw Error(err);
      }
      db.get(args.key, async (err: any, value: Buffer) => {
        if (err) {
          vorpal.log(err);
          callback();
        }
        if (!err) {
          const data = JSON.parse(value.toString());
          await fs.writeFile(args.path, JSON.stringify(data), 'utf8');
          vorpal.log(`[+] Value dump successful`);
        }
        // const json = util.inspect(JSON.parse(value.toString()), {showHidden: false, depth: null, colors: false});
        // vorpal.log(json);
      });
      db.close(() => {});
    });
    callback();
  });
vorpal.delimiter('rocksdb$').show();
